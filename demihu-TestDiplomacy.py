# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve# diplomacy.py functions here

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    #def test_read(self):
    #    s = "1 10\n"
    #    i, j = collatz_read(s)
    #    self.assertEqual(i,  1)
    #    self.assertEqual(j, 10)


    # ----
    # eval
    # ----

    #def test_eval_1(self):
    #    v = collatz_eval(1, 10)
    #    self.assertEqual(v, 20)

    # -----
    # print
    # -----

    #def test_print(self):
    #    w = StringIO()
    #    collatz_print(w, 1, 10, 20)
    #self.assertEqual(w.getvalue(), "1 10 20\n")


    # -----
    # solve
    # -----
  
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n") 
            
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n") 
            
    def test_solve_3(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n") 
            
    def test_solve_4(self): # case: unsorted
        r = StringIO("B Madrid Hold\nA Barcelona Move London\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\n") 
    
    

# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()
    
