from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    def test_read(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a, [('A', 'Madrid', 'Hold', None)])

    def test_read_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London"
        a = diplomacy_read(s)
        self.assertEqual(a, [('A', 'Madrid', 'Hold', None), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Support', 'B'), ('D', 'Austin', 'Move', 'London')])

    def test_read_3(self):
        s = "C London Move Paris\n"
        a = diplomacy_read(s)
        self.assertEqual(a, [('C', 'London', 'Move', 'Paris')])

    # ----
    # eval
    # ----

    # given unit tests
    def test_eval_1(self):
        actions = [('A', 'Madrid', 'Hold', None), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Support', 'B')]
        case = diplomacy_eval(actions)
        self.assertEqual(case, ['A [dead]\n', 'B Madrid\n' , 'C London\n'])

    def test_eval_2(self):
        actions = [('A', 'Madrid', 'Hold', None), ('B', 'Barcelona', 'Move', 'Madrid')]
        case = diplomacy_eval(actions)
        self.assertEqual(case, ['A [dead]\n', 'B [dead]\n'])

    def test_eval_3(self):
        actions = [('A', 'Madrid', 'Hold', None), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Support', 'B'), ('D', 'Austin', 'Move', 'London')]
        case = diplomacy_eval(actions)
        self.assertEqual(case, ['A [dead]\n', 'B [dead]\n', 'C [dead]\n', 'D [dead]\n'])

    def test_eval_4(self):
        actions = [('A', 'Madrid', 'Hold', None), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Move', 'Madrid')]
        case = diplomacy_eval(actions)
        self.assertEqual(case, ['A [dead]\n', 'B [dead]\n', 'C [dead]\n'])

    def test_eval_5(self):
        actions = [('A', 'Madrid', 'Hold', None), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Move', 'Madrid'), ('D', 'Paris', 'Support', 'B')]
        case = diplomacy_eval(actions)
        self.assertEqual(case, ['A [dead]\n','B Madrid\n', 'C [dead]\n', 'D Paris\n'])

    def test_eval_6(self):
        actions = [('A', 'Madrid', 'Hold', None), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Move', 'Madrid'), ('D', 'Paris', 'Support', 'B'), ('E', 'Austin', 'Support', 'A')]
        case = diplomacy_eval(actions)
        self.assertEqual(case, ['A [dead]\n', 'B [dead]\n', 'C [dead]\n', 'D Paris\n', 'E Austin\n'])
    
    def test_eval_7(self):
        actions = [('A', 'Madrid', 'Hold', None)]
        case = diplomacy_eval(actions)
        self.assertEqual(case, ['A Madrid\n'])

    '''
    implement self created unit tests here
    def test_eval_8(self):
        actions = [('A', 'Madrid', 'Hold', None)]
        case = diplomacy_eval(actions)
        self.assertEqual(case, {'A Madrid'})

    def test_eval_9(self):
        actions = [('A', 'Madrid', 'Hold', None)]
        case = diplomacy_eval(actions)
        self.assertEqual(case, {'A Madrid'})

    def test_eval_10(self):
        actions = [('A', 'Madrid', 'Hold', None)]
        case = diplomacy_eval(actions)
        self.assertEqual(case, {'A Madrid'})
    '''
    

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    def test_print4(self):
        w = StringIO()
        diplomacy_print(w, 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    def test_print5(self):
        w = StringIO()
        diplomacy_print(w, 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

#----
# main
# ----

if __name__ == "__main__":
    main()

''' #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
....................................
----------------------------------------------------------------------
Ran 36 tests in 0.005s

OK


$ coverage report -m                   >> TestDiplomacy.out


$ cat TestDiplomacy.out
....................................
----------------------------------------------------------------------
Ran 36 tests in 0.005s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          96      0     72      0   100%
TestDiplomacy.py     156      0      2      1    99%   222->exit
--------------------------------------------------------------
TOTAL                252      0     74      1    99%
'''