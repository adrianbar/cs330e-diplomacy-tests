#!/usr/bin/env python3

# -------------------------------
#
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        a = diplomacy_read(s)
        self.assertEqual(a, ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B', 'D Austin Move London'])

    def test_read2(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        a = diplomacy_read(s)
        self.assertEqual(a, ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A'])
        
    def test_read3(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Paris Support C")
        a = diplomacy_read(s)
        self.assertEqual(a, ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B', 'D Austin Move London', 'E Paris Support C'])
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B', 'D Austin Move London'])
        self.assertEqual(v, ["A [dead]", 'B [dead]', 'C [dead]', 'D [dead]'])

    def test_eval_2(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A'])
        self.assertEqual(v, ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin'])
    
    def test_eval_3(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B', 'D Austin Move London', 'E Paris Support C'])
        self.assertEqual(v, ['A [dead]', 'B [dead]', 'C London', 'D [dead]', 'E Paris'])

    
    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", 'B [dead]', 'C [dead]', 'D [dead]'])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin'])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B [dead]', 'C London', 'D [dead]', 'E Paris'])
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC London\nD [dead]\nE Paris\n')

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
            
    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Paris Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE Paris\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

