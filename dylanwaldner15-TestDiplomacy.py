from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    
    def test_read_1(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ['A', 'Madrid', ['Hold']])

    def test_read_1_corner(self):
        s = "\nA Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ['A', 'Madrid', ['Hold']])
    # def test_read_1_failure(self):
        # s = "A Madrid Hold\n"
        # i = diplomacy_read(s)
        # self.assertEqual(i, ["B Barcelona Move"])

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ['B', 'Barcelona', ['Move', 'Madrid']])

    def test_read_2_corner(self):
        s = "B Barcelona Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ['B', 'Barcelona', ['Move', 'Madrid']])
    # def test_read_2_failure(self):
        # s = "A Madrid Hold\nB Barcelona Move Madrid\n"
        # i = diplomacy_read(s)
        # self.assertEqual(i, ["B Barcelona Move Madrid", "A Madrid Hold"])

    # ----
    # eval
    # ----

    def test_eval_0(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"]])
        self.assertEqual(v, [['A', '[dead]'], ['B', 'Madrid'], ['C', 'London']])

    def test_eval_1(self):
        v = diplomacy_eval([["B", "Barcelona", "Hold"]])
        self.assertEqual(v, [["B", "Barcelona"]])
    
    def test_eval_2(self):
        v = diplomacy_eval([['A', 'Paris', "Hold"], ['B', 'London', "Hold"], ['C', 'Madrid', "Move", "Paris"], ['D', 'Berlin', "Move", "Paris"], ['E', 'NewYork', "Support", "C"], ['F', 'Austin', "Move", "Paris"], ['G', 'Chicago', "Support", "D"], ['H', 'Boston', "Move", "NewYork"], ['I', 'Toronto', "Move", "NewYork"], ['J', 'Montreal', "Support", "I"], ['K', 'SanFrancisco', "Move", "NewYork"]])

        self.assertEqual(v, [["A", "[dead]"], ["B", "London"], ["C", "[dead]"],["D", "Paris"], ["E", "[dead]"], ["F", "[dead]"], ["G", "Chicago"], ["H", "[dead]"], ["I", "NewYork"], ["J", "Montreal"], ["K", "[dead]"]] )
    # -----
    # print
    # -----

    def test_eval_3(self):
        v = diplomacy_eval([['A', 'Paris', "Hold"], ['B', 'London', "Move", "Paris"], ['C', 'Madrid', "Move", "Paris"], ['D', 'Berlin', "Move", "Paris"], ['E', 'NewYork', "Move", "Paris"], ['F', 'Austin', "Support", "C"], ['G', 'Chicago', "Support", "C"], ['H', 'Boston', "Support", "B"], ['I', 'Toronto', "Support", "A"], ['J', 'Montreal', "Support", "A"], ['K', 'SanFrancisco', "Support", "B"], ['L', 'Sydney', "Support", "A"]])
        self.assertEqual(v, [["A", "Paris"], ["B", "[dead]"], ["C", "[dead]"], ["D", "[dead]"], ["E", "[dead]"], ["F", "Austin"], ["G", "Chicago"], ["H", "Boston"], ["I", "Toronto"], ["J", "Montreal"], ["K", "SanFrancisco"], ["L", "Sydney"]]) 
 

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"]])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    #def test_print_1_corner(self):
     #   w = StringIO()
      #  diplomacy_print(w, ["", "A Madrid"])
       # self.assertEqual(w.getvalue(), "A Madrid\n")
    # def test_print_1_failure(self):
        # w = StringIO()
        # diplomacy_print(w, ["A Madrid"])
        # self.assertEqual(w.getvalue(), "B Barcelona\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"], ["B", "Barcelona"]])
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\n")

    #def test_print_2_corner(self):
     #   w = StringIO()
      #  diplomacy_print(w, ["B Barcelona", "A Madrid"])
       # self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\n")
    # def test_print_2_failure(self):
        # w = StringIO()
        # diplomacy_print(w, ["A Madrid", "B Barcelona"])
        # self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"], ["B", "Barcelona"], ["C", "London"]])
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")

    #def test_print_3_corner(self):
     #   w = StringIO()
      #  diplomacy_print(w, ["London C", "B Barcelona", "A Madrid"])
       # self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nLondon C\n")
    # def test_print_3_failure(self):
        # w = StringIO()
        # diplomacy_print(w, ["A Madrid", "B Barcelona", "C London"])
        # self.assertEqual(w.getvalue(), "C London\nA Madrid\n B Barcelona")
    
    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        what = w.getvalue()
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_1_corner(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
    # def test_solve_1_failure(self):
        # r = ListIO(["A Madrid Hold"])
        # w = ListIO()
        # diplomacy_solve(r, w)
        # self.assertEqual(
        # w.getvalue(), ["B Barcelona"])

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_2_corner(self):
        r = StringIO("B Barcelona Move Madrid\nA Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")
    # def test_solve_2_failure(self):
        # r = ListIO(["A Madrid Hold", "B Barcelona Move Madrid"])
        # w = ListIO()
        # diplomacy_solve(r, w)
        # self.assertEqual(
        # w.getvalue(), ["C Barcelona"])

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3_corner(self):
        r = StringIO("C London Support B\nB Barcelona Move Madrid\nA Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    # def test_solve_3_failure(self):
        # r = ListIO(["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"])
        # w = ListIO()
        # diplomacy_solve(r, w)
        # self.assertEqual(
        # w.getvalue(), ["A Barcelona", [dead], "C Madrid")

    def test_solve_4(self):
        r = StringIO("A Paris Hold\nB London Hold\nC Madrid Move Paris\nD Berlin Move Paris\nE NewYork Support C\nF Austin Move Paris\nG Chicago Support D\nH Boston Move NewYork\nI Toronto Move NewYork\nJ Montreal Support I\nK SanFrancisco Support F\nL Sydney Support F")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB London\nC [dead]\nD [dead]\nE [dead]\nF Paris\nG Chicago\nH [dead]\nI NewYork\nJ Montreal\nK SanFrancisco\nL Sydney\n")         
# ----
# main
# ----


if __name__ == "__main__":
    main()

