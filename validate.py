import glob
import os.path
import sys

files = glob.glob("*")
bad = False
for f in files:
  _, ext = os.path.splitext(f)
  if ext not in [".out", ".in", ".py", ".md"]:
    print("Invalid file name ", f)
    bad = True
if bad:
  sys.exit(1)

